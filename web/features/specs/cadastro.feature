#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome   | email                | senha  |
            | Laissa | laybeatriz@gmail.com | 123abc |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input | email_input          | senha_input | mensagem_output                  |
            |            | laybeatriz@gmail.com | 123l        | Oops. Informe seu nome completo! |
            | Laissa     |                      | 123l        | Oops. Informe um email válido!   |
            | Laissa     | laybeatriz&gmail.com | 12sa        | Oops. Informe um email válido!   |
            | Laissa     | laybeatrizg*mail.com | 12sa        | Oops. Informe um email válido!   |
            | Laisa      | laybeatriz@gmail.com |             | Oops. Informe sua senha secreta! |

